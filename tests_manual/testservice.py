from pelops.abstractmicroservice import AbstractMicroservice


class TestService(AbstractMicroservice):
    _version = 0.1
    _monitoring_agent = None

    def __init__(self, config, mqtt_client=None, logger=None, stdout_log_level=None, no_gui=None):
        """
        Constructor - creates the services and the tasks

        :param config: config yaml structure
        :param mqtt_client: mqtt client instance
        :param logger: logger instance
        """
        AbstractMicroservice.__init__(self, config, "testservice", mqtt_client, logger, self.__class__.__name__,
                                      stdout_log_level=stdout_log_level, no_gui=no_gui)

    def _handler(self, message):
        self._logger.info("received messages - publishing message")
        self._mqtt_client.publish("/test/service/pub", message)

    def _start(self):
        self._mqtt_client.subscribe("/test/service/sub", self._handler)

    def _stop(self):
        self._mqtt_client.unsubscribe("/test/service/sub", self._handler)

    def config_information(self):
        return {"config_information":"config_information"}

    def runtime_information(self):
        return {"runtime_information":"runtime_information"}

    @classmethod
    def _get_description(cls):
        return "test service for hippodamia"

    @classmethod
    def _get_schema(cls):
        return {
            "testservice": {
            }
        }


def standalone():
    TestService.standalone()


if __name__ == "__main__":
    TestService.standalone()
